package modele.dao;

import modele.metier.*;
import modele.jdbc.Jdbc;
import java.sql.*;
import java.util.*;

/**
 * Classe DAO pour la classe TypePraticien
 *
 * @version 1.0
 * @author Grenier Lysandra
 */
public class DaoTypePraticien implements DaoInterface<TypePraticien, Integer> {

    /**
     * 
     * @param unTypePraticien
     */
    @Override
    public int create(TypePraticien unTypePraticien) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Méthode getOne()
     * Lire un enregistrement d'après son identifiant
     *
     * @param idTypePraticien métier de l'objet recherché
     * @return objet métier trouvé, ou null sinon
     * @throws modele.dao.DaoException
     */
    @Override
    public TypePraticien getOne(Integer idTypePraticien) throws DaoException {
        TypePraticien result = null;
        ResultSet rs = null;
        // préparer la requête
        String requete = "SELECT * FROM TYPE_PRATICIEN WHERE TYP_CODE=?";
        try {
            PreparedStatement ps = Jdbc.getInstance().getConnexion().prepareStatement(requete);
            ps.setInt(1, idTypePraticien);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = chargerUnEnregistrement(rs);
            }
        } catch (SQLException ex) {
            throw new modele.dao.DaoException("DaoTypePraticien::getOne : erreur requete SELECT : " + ex.getMessage());
        }
        return (result);
    }

    /**
     * Méthode getAll()
     *
     * @return ArrayList de l'ensemble des occurences de TypePraticien de la table
     * TYPE_PRATICIEN
     * @throws modele.dao.DaoException
     */
    @Override
    public Collection<TypePraticien> getAll() throws DaoException {
        ArrayList<TypePraticien> result = new ArrayList<TypePraticien>();
        ResultSet rs;
        // préparer la requête
        String requete = "SELECT * FROM TYPE_PRATICIEN";
        try {
            PreparedStatement ps = Jdbc.getInstance().getConnexion().prepareStatement(requete);
            rs = ps.executeQuery();
            // Charger les enregistrements dans la collection
            while (rs.next()) {
                TypePraticien unTypePraticien = chargerUnEnregistrement(rs);
                result.add(unTypePraticien);
            }
        } catch (SQLException ex) {
            throw new modele.dao.DaoException("DaoTypePraticien::getAll : erreur requete SELECT : " + ex.getMessage());
        }
        return result;
    }

    /**
     * Non implémenté pour l'instant
     */
    @Override
    public int update(Integer idMetier, TypePraticien objetMetier) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Non implémenté pour l'instant
     */
    @Override
    public int delete(Integer idMetier) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    //----------------------------------------------------------------------
    //  Méthodes privées
    //----------------------------------------------------------------------
    /**
     * chargerUnEnregistrementTypePraticien() Instancie un objet secteur avec les
     * valeurs lues dans la base de données La jointure avec la table PRESENCE
     * n'est pas effectuée
     *
     * @param rs enregistrement de la table TypePraticien courant
     * @return un objet TypePraticien, dont la liste des "présences" n'est pas
     * renseignée
     * @throws DaoException
     */
    private TypePraticien chargerUnEnregistrement(ResultSet rs) throws DaoException {
        try {
            TypePraticien typePraticien = new TypePraticien(null,null,null);
            typePraticien.setCode(rs.getString("TYP_CODE"));
            typePraticien.setLibelle(rs.getString("TYP_LIBELLE"));
            typePraticien.setLieu(rs.getString("TYP_LIEU"));
            
            return typePraticien;
        } catch (SQLException ex) {
            throw new DaoException("DaoTypePraticien - chargerUnEnregistrement : pb JDBC\n" + ex.getMessage());
        }
    }
}
