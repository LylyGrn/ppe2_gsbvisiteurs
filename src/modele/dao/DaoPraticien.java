package modele.dao;

import modele.metier.*;
import modele.jdbc.Jdbc;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe DAO pour la classe Praticien
 *
 * @version 1.0
 * @author Grenier Lysandra
 */
public class DaoPraticien implements DaoInterface<Praticien, Integer> {
    private DaoTypePraticien daoTypePraticien = new DaoTypePraticien();
    
    /**
     * 
     * @param unPraticien
     */
    @Override
    public int create(Praticien unPraticien) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    /**
     * Méthode getOne()
     * Lire un enregistrement d'après son identifiant
     * 
     * @param idPraticien métier de l'objet recherché
     * @return
     * @throws modele.dao.DaoException
     */
    @Override
    public Praticien getOne(Integer idPraticien) throws DaoException {
        Praticien result = null;
    try {
        Jdbc.getInstance().connecter();
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(DaoPraticien.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex) {
        Logger.getLogger(DaoPraticien.class.getName()).log(Level.SEVERE, null, ex);
    }
        ResultSet rs = null;
        // préparer la requête
        String requete = "SELECT * FROM PRATICIEN WHERE ID_PRATICIEN=?";
        
        try {
            PreparedStatement ps = Jdbc.getInstance().getConnexion().prepareStatement(requete);
            ps.setInt(1, idPraticien);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = chargerUnEnregistrement(rs);
            }
         
        } catch (SQLException e) {
           
            throw new modele.dao.DaoException("DaoPraticien::getOne : erreur requete SELECT : " + e.getMessage());
        }
    try {
        Jdbc.getInstance().deconnecter();
    } catch (SQLException ex) {
        Logger.getLogger(DaoPraticien.class.getName()).log(Level.SEVERE, null, ex);
    }
        return (result);
    }

    /**
     * Méthode getAll()
     *
     * @return ArrayList de l'ensemble des occurences des praticiens de la table
     * PRATICIEN
     * @throws modele.dao.DaoException
     */
    @Override
    public ArrayList<Praticien> getAll() throws DaoException {
        ArrayList<Praticien> result = new ArrayList<>();
    try {
        Jdbc.getInstance().connecter();
    } catch (ClassNotFoundException ex) {
        Logger.getLogger(DaoPraticien.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SQLException ex) {
        Logger.getLogger(DaoPraticien.class.getName()).log(Level.SEVERE, null, ex);
    }
        ResultSet rs;
        
        // préparer la requête
        String requete = "SELECT * FROM PRATICIEN";
        Jdbc.getInstance().getConnexion();
        try {
 
				
            PreparedStatement ps = Jdbc.getInstance().getConnexion().prepareStatement(requete);
            rs = ps.executeQuery();
            // Charger les enregistrements dans la collection
            while (rs.next()) {
                Praticien unPraticien = chargerUnEnregistrement(rs);
                result.add(unPraticien);
            }
            
        } catch (SQLException e) {
            throw new modele.dao.DaoException("DaoPraticien::getOne : erreur requete SELECT : " + e.getMessage());
        }
    try {        
     
  
  
        Jdbc.getInstance().deconnecter();
    } catch (SQLException ex) {
        Logger.getLogger(DaoVisiteur.class.getName()).log(Level.SEVERE, null, ex);
    }
        return result;
        
    }

    /**
     * Non implémenté pour l'instant 
     */
    @Override
    public int update(Integer idMetier, Praticien objetMetier) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Non implémenté pour l'instant 
     */
    @Override
    public int delete(Integer idMetier) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    //----------------------------------------------------------------------
    //  Méthodes privées
    //----------------------------------------------------------------------
    /**
     * chargerUnEnregistrementPraticien() Instancie un objet praticien avec les
     * valeurs lues dans la base de données La jointure avec la table PRESENCE
     * n'est pas effectuée
     *
     * @param rs enregistrement de la table Praticien courant
     * @return un objet Praticien, dont la liste des "présences" n'est pas
     * renseignée
     * @throws DaoException
     */
    private Praticien chargerUnEnregistrement(ResultSet rs) throws DaoException {
        try {
            Praticien praticien = new Praticien((null),null,null,null,null,null,null,null);
            praticien.setNum(rs.getInt("PRA_NUM"));
            praticien.setPrenom(rs.getString("PRA_PRENOM"));
            praticien.setNom(rs.getString("PRA_NOM"));
            praticien.setAdresse(rs.getString("PRA_ADRESSE"));
            praticien.setVille(rs.getString("PRA_VILLE"));
            praticien.setCp(rs.getString("PRA_CP"));
            praticien.setCoefnotoriete(rs.getDouble("PRA_COEFNOTORIETE"));
            praticien.setTypePraticien(daoTypePraticien.getOne(rs.getInt("TYP_CODE")));
            return praticien;
        } catch (SQLException ex) {
            throw new DaoException("DaoPraticien - chargerUnEnregistrement : pb JDBC\n" + ex.getMessage());
        }
    }
}
