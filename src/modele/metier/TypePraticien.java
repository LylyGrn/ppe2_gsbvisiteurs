package modele.metier;

/**
 * 
 * Classe Famille 
 * @author Grenier Lysandra
 */
public class TypePraticien {
    
    //Variables 
    private String code;
    private String libelle;
    private String lieu;

    /**
     * Constructeur de la classe TypePraticien
     * @param code
     * @param libelle
     * @param lieu 
     */
    public TypePraticien(String code, String libelle, String lieu) {
        this.code = code;
        this.libelle = libelle;
        this.lieu = lieu;
    }
    
    //Getters et Setters

    /**
     * Méthode getCode()
     * @return retourne le code du type de praticien
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Méthode getLibelle()
     * @return retourne le libelle du type de praticien
     */
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Méthode getLieu()
     * @return retourne le type de lieu d'exercice des fonctions
     */
    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }
    
}
