package modele.metier;

/**
 *
 * Classe Visiteur
 * @author Grenier Lysandra
 */
public class Visiteur {
    
    //Variables 
    private String matricule;
    private String nom;
    private String prenom;
    private String adresse;
    private String cp;
    private String ville;
    private String dateEmbauche;
    private Secteur secteur;
    private Labo labo;

    /**
     * Constructeur de la classe Visiteur
     * @param matricule
     * @param nom
     * @param prenom
     * @param adresse
     * @param cp
     * @param ville
     * @param dateEmbauche
     * @param secteur
     * @param labo 
     */
    public Visiteur(String matricule, String nom, String prenom, String adresse, String cp, String ville, String dateEmbauche, Secteur secteur, Labo labo) {
        this.matricule = matricule;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.cp = cp;
        this.ville = ville;
        this.dateEmbauche = dateEmbauche;
        this.secteur = secteur;
        this.labo = labo;
    }

    @Override
    /**
     * Méthode toString()
     * @return retourne le nom et le prénom d'un visiteur sous la forme "nom prénom"
     */
    public String toString() {
        return  nom + " " + prenom;
    }

    //Getters et Setters
    
    /**
     * Méthode getMatricule()
     * @return retourne le matricule d'un visiteur
     */
    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    /**
     * Méthode getNom()
     * @return Retourne le nom du visiteur
     */
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode getPrenom()
     * @return Retourne le prénom du visiteur
     */
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Méthode getAdresse()
     * @return Retourne l'adresse du visiteur
     */
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Méthode getCp()
     * @return Retourne le code postal du visiteur
     */
    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    /**
     * Méthode getVille()
     * @return Retourne la ville du visiteur
     */
    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * Méthode getDateEmbauche()
     * @return Retourne la date d'embauche du visiteur
     */
    public String getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(String dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    /**
     * Méthode getSecteur()
     * @return Retourne le secteur du visiteur
     */
    public Secteur getSecteur() {
        return secteur;
    }

    public void setSecteur(Secteur secteur) {
        this.secteur = secteur;
    }

    /**
     * Méthode getLabo()
     * @return Retourne le laboratoire du visiteur
     */
    public Labo getLabo() {
        return labo;
    }

    public void setLabo(Labo labo) {
        this.labo = labo;
    }

}