package modele.metier;

/**
 *
 * Classe Secteur
 * @author Grenier Lysandra
 */
public class Secteur {
    
    //Variables
    private String code;
    private String libelle ;

    /**
     * Constructeur de la classe Secteur
     * @param code
     * @param libelle 
     */
    public Secteur(String code, String libelle) {
        this.code = code;
        this.libelle = libelle;
    }

    @Override
    /**
     * Méthode toString()
     * @return retourne le libellé du secteur
     */
    public String toString() {
        return libelle;
    }

    //Getters et Setters
    
    /**
     * Méthode getCode()
     * @return retroune le code d'un secteur
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Méthode getLibelle()
     * @return retroune le libellé d'un secteur
     */
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    
}
