package modele.metier;

/**
 *
 * Classe RapportVisite
 * @author Grenier Lysandra
 */
public class RapportVisite {
    
    //Variables
    private Visiteur visiteur;
    private int num;
    private Praticien praticien;
    private String date;
    private String bilan;
    private String motif;

    /**
     * Constructeur de la classe RapportVisite
     * @param visiteur
     * @param num
     * @param praticien
     * @param date
     * @param bilan
     * @param motif 
     */
    public RapportVisite(Visiteur visiteur, int num, Praticien praticien, String date, String bilan, String motif) {
        this.visiteur = visiteur;
        this.num = num;
        this.praticien = praticien;
        this.date = date;
        this.bilan = bilan;
        this.motif = motif;
    }

    @Override
    /**
     * Méthode toString()
     * @return retourne le numéro et la date d'un rapport de visite sous la forme "numéro - date"
     */
    public String toString() {
        return num + "-" + date;
    }
    
    //Getters et Setters

    /**
     * Méthode getVisiteur()
     * @return retourne le visiteur
     */
    public Visiteur getVisiteur() {
        return visiteur;
    }

    public void setVisiteur(Visiteur visiteur) {
        this.visiteur = visiteur;
    }

    /**
     * Méthode getNum()
     * @return retourne le numéro du rapport de visite
     */
    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    /**
     * Méthode getPraticien()
     * @return retourne le praticien
     */
    public Praticien getPraticien() {
        return praticien;
    }

    public void setPraticien(Praticien praticien) {
        this.praticien = praticien;
    }

    /**
     * Méthode getDate()
     * @return retourne la date du rapport de visite
     */
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Méthode getBilan()
     * @return retourne le bilan du rapport de visite
     */
    public String getBilan() {
        return bilan;
    }

    public void setBilan(String bilan) {
        this.bilan = bilan;
    }

    /**
     * Méthode getMotif()
     * @return retourne le motif du rapport de visite
     */
    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }
    
}
