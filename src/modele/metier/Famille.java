package modele.metier;

/**
 * 
 * Classe Famille 
 * @author Grenier Lysandra
 */
public class Famille {
    
    //Variables
    private String code;
    private String libelle;

    /**
     * Constructeur de la classe Famille
     * @param code
     * @param libelle 
     */
    public Famille(String code, String libelle) {
        this.code = code;
        this.libelle = libelle;
    }

    @Override
    /**
     * @return retourne le libellé de la famille de médicament sous la forme "libellé"
     */
    public String toString() {
        return libelle;
    }
    
    //Getters et Setters

    /**
     * Méthode getCode()
     * @return retourne le code de la famille de médicament
     */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Méthode getLibelle()
     * @return retourne le libellé de la famille de médicament
     */
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    
    
    
}
