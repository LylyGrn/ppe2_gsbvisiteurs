package modele.metier;

/**
 *
 * Classe Praticien
 * @author Grenier Lysandra
 */
public class Praticien {
    
    //Variables
    private int num;
    private String nom;
    private String prenom;
    private String adresse;
    private String cp;
    private String ville;
    private double coefnotoriete;
    private TypePraticien typePraticien;

    /**
     * Constructeur de la classe Praticien
     * @param num
     * @param nom
     * @param prenom
     * @param adresse
     * @param cp
     * @param ville
     * @param coefnotoriete
     * @param typePraticien 
     */
    public Praticien(int num, String nom, String prenom, String adresse, String cp, String ville, double coefnotoriete, TypePraticien typePraticien) {
        this.num = num;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.cp = cp;
        this.ville = ville;
        this.coefnotoriete = coefnotoriete;
        this.typePraticien = typePraticien;
    }

    @Override
    /**
     * Méthode toString()
     * @return retourne le nom et le prénom d'un praticien sous la forme "nom prénom"
     */
    public String toString() {
        return  nom + " " + prenom;
    }
    
    //Getters et Setters
    
    /**
     * Méthode getNum()
     * @return retourne le numéro du praticien
     */
    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    /**
     * Méthode getNom()
     * @return retourne le nom du praticien
     */
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode getPrenom()
     * @return retourne le prénom du praticien
     */
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Méthode getAdresse()
     * @return retourne l'adresse du praticien
     */
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Méthode getCp()
     * @return retourne le code postal du praticien
     */
    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    /**
     * Méthode getVille()
     * @return retourne la ville du praticien
     */
    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * Méthode getCoefnotoriete()
     * @return retourne le coefficient de notoriété du praticien
     */
    public double getCoefnotoriete() {
        return coefnotoriete;
    }

    public void setCoefnotoriete(double coefnotoriete) {
        this.coefnotoriete = coefnotoriete;
    }

    /**
     * Méthode getTypePraticien()
     * @return retourne le type de praticien
     */
    public TypePraticien getTypePraticien() {
        return typePraticien;
    }

    public void setTypePraticien(TypePraticien type) {
        this.typePraticien = typePraticien;
    }
    
    
    
}
