package modele.metier;

/**
 *
 * Classe Medicament
 * @author Grenier Lysandra
 */
public class Medicament {
    
    //Variables
    private String depotlegal;
    private String nomcommercial;
    private Famille famille;
    private String composition;
    private String effets;
    private String contreindic;
    private String prixechantillon;
    
    /**
     * Constructeur de la classe Medicament
     * @param depotlegal
     * @param nomcommercial
     * @param famille
     * @param composition
     * @param effets
     * @param contreindic
     * @param prixechantillon 
     */
    public Medicament(String depotlegal, String nomcommercial, Famille famille, String composition, String effets, String contreindic, String prixechantillon) {
        this.depotlegal = depotlegal;
        this.nomcommercial = nomcommercial;
        this.famille = famille;
        this.composition = composition;
        this.effets = effets;
        this.contreindic = contreindic;
        this.prixechantillon = prixechantillon;
    }

    @Override
    /**
     * Méthode toString()
     * @return retourne le nom du médicament sous la forme "nom"
     */
    public String toString() {
        return nomcommercial;
    }
    
    //Getters et Setters

    /**
     * Méthode getDepotlegal()
     * @return retourne le dépôt légal
     */
    public String getDepotlegal() {
        return depotlegal;
    }

    public void setDepotlegal(String depotlegal) {
        this.depotlegal = depotlegal;
    }

    /**
     * Méthode getNomcommercial()
     * @return retourne le nom commercial du médicament
     */
    public String getNomcommercial() {
        return nomcommercial;
    }

    public void setNomcommercial(String nomcommercial) {
        this.nomcommercial = nomcommercial;
    }

    /**
     * Méthode getFamille()
     * @return retourne la famille de médicament
     */
    public Famille getFamille() {
        return famille;
    }

    public void setFamille(Famille famille) {
        this.famille = famille;
    }

    /**
     * Méthode getComposition()
     * @return retourne la composition du médicament
     */
    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    /**
     * Méthode getEffets()
     * @return retourne les effets secondaires du médicament
     */
    public String getEffets() {
        return effets;
    }

    public void setEffets(String effets) {
        this.effets = effets;
    }

    /**
     * Méthode getContreindic()
     * @return retourne les contres indications du médicament
     */
    public String getContreindic() {
        return contreindic;
    }

    public void setContreindic(String contreindic) {
        this.contreindic = contreindic;
    }

    /**
     * Méthode getPrixechantillon()
     * @return retourne le prix d'un échantillon
     */
    public String getPrixechantillon() {
        return prixechantillon;
    }

    public void setPrixechantillon(String prixechantillon) {
        this.prixechantillon = prixechantillon;
    }
    
    
    
}
