package modele.metier;

/**
 * 
 * Classe Labo 
 * @author Grenier Lysandra
 */
public class Labo {
    
    //Variables
    private String code;
    private String nom;
    private String chefVente;

    /**
     * Constructeur de la classe Labo
     * @param code
     * @param nom
     * @param chefVente 
     */
    public Labo(String code, String nom, String chefVente) {
        this.code = code;
        this.nom = nom;
        this.chefVente = chefVente;
    }
    
    /**
     *  Constructeur sans paramètres de la classe Labo
     */
    public Labo() {
        this.code = "";
        this.nom = "";
        this.chefVente = "";
    }
    
    
    //Getters et Setters 
    
    /**
     * Méthode getCode()
     * @return retourne le code du laboratoire
     */
      public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Méthode getNom()
     * @return retourne le nom du laboratoire
     */
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode getChefVente()
     * @return retourne le chef de vente du laboratoire
     */
    public String getChefVente() {
        return chefVente;
    }

    public void setChefVente(String chefVente) {
        this.chefVente = chefVente;
    }

    @Override
    /**
     * Méthode toString()
     * @return retourne le nom du laboratoire
     */
    public String toString() {
        return nom ;
    }
    
    
    
}