package controleur;

/**
 * Liste des actions prises en charge par le contrôleur frontal
 * Règles de nommage pour ajouter une action :
 * - en majuscules
 * - le nom du contrôleur suivi d'un tiret bas et du nom de l'action pour ce contrôleur
 * @author Grenier Lysandra 
 * @version 1.0
 */
public enum EnumAction {
    MENU_VISITEUR
    ,VISITEUR_FERMER
    ,MENU_QUITTER
    ,VISITEUR_SUIVANT
    ,VISITEUR_PRECEDENT
    
    ,MENU_PRATICIEN
    ,PRATICIEN_FERMER
    ,PRATICIEN_SUIVANT
    ,PRATICIEN_PRECEDENT
}
