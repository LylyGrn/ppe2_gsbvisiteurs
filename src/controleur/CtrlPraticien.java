package controleur;


import java.util.List;
import javax.swing.JOptionPane;
import modele.dao.*;
import modele.metier.*;
import vue.VuePraticien;


/**
 * Contrôleur de la fenêtre VuePraticien
 * @author Grenier Lysandra
 * @version 1.0
 * 
 */
public class CtrlPraticien extends CtrlAbstrait {
    
    private DaoPraticien daoPraticien = new DaoPraticien();
    
    /**
     * Constructeur de la classe CtrlPraticien
     * @param ctrlPrincipal 
     */
    public CtrlPraticien(CtrlPrincipal ctrlPrincipal) {
        super(ctrlPrincipal);
        vue = new VuePraticien(this);
        actualiser();
    }
    
    
    public final void actualiser() {
        try {
            chargerListePraticiens();
        } catch (DaoException ex) {
            JOptionPane.showMessageDialog(getVue(), "CtrlPraticien - actualiser - " + ex.getMessage(), "", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * praticienFermer réaction au clic sur le bouton Fermer de la vuePraticien 
     * Le contrôle est rendu au contrôleur frontal
     */
    public void praticienFermer() {
        this.getCtrlPrincipal().action(EnumAction.PRATICIEN_FERMER);
    }
   /**Bouton suivant. rajoute +1 a chaque utilisateur 
    * 
    */
    public void praticienSuivant(){
        int index = getVue().getjComboBoxPraticien().getSelectedIndex()+1;
        if(index== getVue().getjComboBoxPraticien().getItemCount())index=0;
        getVue().getjComboBoxPraticien().setSelectedIndex(index);
    }
    
    /**
     Bouton précédent enleve -1 a chaque utilisateur
     */
    public void praticienPrecedent(){
        int index = getVue().getjComboBoxPraticien().getSelectedIndex()-1;
        if(index== -1) index=getVue() .getjComboBoxPraticien() .getItemCount() -1;
        getVue().getjComboBoxPraticien().setSelectedIndex(index);
    }
    /**
     * chargerListePraticiens renseigner le modèle du composant jComboBoxPraticien
     * à partir de la base de données
     *
     * @throws DaoException
     */
    private void chargerListePraticiens() throws DaoException {
        List<Praticien> desPraticiens = daoPraticien.getAll();
        getVue().getModeleJComboBoxPraticien().removeAllElements();
        for (Praticien unPraticien : desPraticiens) {
            getVue().getModeleJComboBoxPraticien().addElement(unPraticien);
        }
    }
    /**
     * praticienSelectionner() renseigner le modèle des composants
     * JTextField (TxtNom, TxtPrenom, TxtAdresse, TxtVille, TxtCp, TxtNum, TxtCoeff)
     *
     */
    public void praticienSelectionner (){
        Praticien praticienSelect = (Praticien) getVue().getjComboBoxPraticien().getSelectedItem();
        if (praticienSelect != null){
            
            getVue().getTxtNom().setText(praticienSelect.getNom());
            getVue().getTxtPrenom().setText(praticienSelect.getPrenom());
            getVue().getTxtAdresse().setText(praticienSelect.getAdresse());
            getVue().getTxtVille().setText(praticienSelect.getVille());
            getVue().getTxtCp().setText(praticienSelect.getCp());
            //getVue().getTxtNum().setText(praticienSelect.getNum());
            //getVue().getTxtCoeff().setText(praticienSelect.getCoefnotoriete));
        }       
    }
   
    @Override
    public VuePraticien getVue() {
        return (VuePraticien) vue;
    }
}
